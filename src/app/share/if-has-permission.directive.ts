import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

//Custom services
import { UserService } from '../services/user.service';

@Directive({
  selector: '[ifHasPermission]'
})
export class IfHasPermissionDirective {

	@Input() set ifHasPermission( permission: string ) {
    if( this.hasPermission( permission )  )
      this.viewContainer.createEmbeddedView( this.templateRef );
    else 
      this.viewContainer.clear( );
  }

	constructor( private user: UserService,  private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef ) { }
 
	private hasPermission( permission: string ){
		if( this.user.hasPermission( permission ) )
			return true;
		return false;
	}
}
