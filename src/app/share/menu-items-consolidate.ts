// custome interface
import { MenuItem } from './menu-item.interface';
//custome menu items
import { HomeMenuItems,HomeToolItems } from '../custom-modules/home/config';

export const menuItemsList: MenuItem[]= [
  ...HomeMenuItems
];

export const toolItemsList: MenuItem[]= [
  ...HomeToolItems
];
