import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


//Custom directives
import { IfHasPermissionDirective } from './if-has-permission.directive';

@NgModule({
  imports: [
    CommonModule
  ],
	declarations: [
		IfHasPermissionDirective 
	]
})
export class ShareModule { }
