 interface  Menu {
  state: string;
  name: string;
  icon: string;
  permission:string;
}
export interface MenuItem extends Menu {
  children?:Menu[]
}
