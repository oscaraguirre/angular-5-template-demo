//Angular imports
import { BrowserModule } 												from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } 						from '@angular/core';
import { FormsModule } 													from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } 	from '@angular/common/http';
import { CookieService } 												from 'ngx-cookie-service';

//Custom modules
import { MaterialModule } 			from './material/material.module';
import { LayoutModule }					from './layout/layout.module';
import { QueuesModule } 				from './queues/queues.module';
import { AppRoutingModule } 		from './app-routing.module';
import { ShareModule } 					from './share/share.module';
import { DefaultPagesModule }		from './default-pages/default-pages.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { HomeModule }           from './custom-modules/home/home.module';

//Custom services
import { HttpRequestsInterceptor } 	from './services/http-requests-interceptor.service';
import { IcresonCommonDataService } from './services/icreson-common-data.service';

//Custom components
import { AppComponent } from './app.component';


@NgModule({
  declarations: [
		AppComponent
  ],
  imports: [
		FormsModule,
		HttpClientModule,
		MaterialModule,
		QueuesModule,
		AppRoutingModule,
		LayoutModule,
		ShareModule,
		DefaultPagesModule,
    AuthenticationModule,
    HomeModule
	],
	providers: [
		CookieService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: HttpRequestsInterceptor,
			multi: true
		},
		{
			provide: APP_INITIALIZER,
			useFactory: ( common: IcresonCommonDataService ) => () => common.loadHosts(),
			deps: [ IcresonCommonDataService ],
			multi: true
		},
		{
			provide: APP_INITIALIZER,
			useFactory: ( common: IcresonCommonDataService ) => () => common.loadServices(),
			deps: [ IcresonCommonDataService ],
			multi: true
		},
		{
			provide: APP_INITIALIZER,
			useFactory: ( common: IcresonCommonDataService ) => () => common.loadOffices(),
			deps: [ IcresonCommonDataService ],
			multi: true
		}
	],
	bootstrap: [
		AppComponent
	]
})
export class AppModule { }
