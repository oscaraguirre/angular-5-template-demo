import { Routes } from '@angular/router';

// custome interface
import { MenuItem } from '../../share/menu-item.interface';

//Local components to route
import {  HomeComponent } from './home/home.component';

export const HomeRoutes: Routes = [
	{
		path: 			'home',
		component: 	HomeComponent
	}
];

export const HomeMenuItems: MenuItem[] = [
  {
    state: "home",
    icon: "home",
    name: "Inicio",
    permission: ""
}

];

export const HomeToolItems: MenuItem[] = [
  {
    state: "home",
    icon: "home",
    name: "Inicio",
    permission: ""
}

];
