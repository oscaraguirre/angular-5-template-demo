import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot } from '@angular/router';

//Custom services
import { UserService } from './user.service';

@Injectable()
export class PermissionGuardService implements CanActivate, CanActivateChild {

  constructor( private user: UserService, private router: Router ) { }

	canActivate( route: ActivatedRouteSnapshot ){
		let mustHasPermission = route.data[ 'permissions' ] as Array<string>;
		for( let p in mustHasPermission )
			if( this.user.hasPermission( mustHasPermission[ p ] ) )
				return true;
			else {
				this.router.navigate( [ '/unauthorized' ] );
				return false;
			}
	}

	canActivateChild( route: ActivatedRouteSnapshot ){
		return this.canActivate( route );
	}
}
