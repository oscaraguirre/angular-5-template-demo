import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } 		from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IcresonCommonDataService {

	static  configURL:	string = 'http://172.30.8.15:3000/repository/';
	private hosts:			any;	
	private services:		any;
	private offices:		any;

	private loaded = new Subject<any>();

  constructor( private http: HttpClient ) { }

	public getHosts(){
		return this.hosts;
	}

	public getOffices(){
		return this.offices;
	}

	public getOffice( id: number ){
		return this.offices ? this.offices[ id ] : null;
	}
	
	public getLoad(){
		return this.loaded.asObservable();
	}

	public getServices(){
		return this.services;
	}

	private allLoaded(){
		if( this.hosts && this.services && this.offices ) 
			this.loaded.complete();
	}

	public loadOffices() :Promise<boolean>{
		return new Promise( ( resolve, reject ) => {
			this.http.get( IcresonCommonDataService.configURL + 'offices/offices.json' )
				.subscribe(
					data 	=> {
						this.offices = data;
						this.allLoaded();
						resolve( true );
					},
					error => {
						console.error( error );
						reject( false );
					}
				)
		} );
	}

	public loadServices() :Promise<boolean>{
		return new Promise( ( resolve, reject ) => {
			this.http.get( IcresonCommonDataService.configURL + 'services.json' )
				.subscribe(
					data 	=> {
						this.services = data;
						this.allLoaded(); 
						resolve( true );
					},
					error => {
						console.error( error );
						reject( false );
					}
				)
		} );
	}

	public loadHosts() :Promise<boolean>{
		return new Promise( ( resolve, reject ) => {
			this.http.get( IcresonCommonDataService.configURL + 'hosts.json' )
				.subscribe(
					data 	=> {
						this.hosts = data;
						this.allLoaded();
						resolve( true );
					},
					error => {
						console.error( error );
						reject( false );
					}
				)
		} );
	}
}
