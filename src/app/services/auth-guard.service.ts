import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';

//Custom services
import { UserService } from './user.service';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

  constructor( private user: UserService, private router: Router ) { }

	canActivate(){
		if( this.user.isLogged() )
			return true;
		else if( this.user.getToken() )
			return this.user.checkToken()
	  else {
			this.router.navigate( [ '/authenticate' ] );
			return false;
		}
	}

	canActivateChild(){
		return this.canActivate();
	}
}
