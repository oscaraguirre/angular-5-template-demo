import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

//Custom services
import { ApplicationService } from './application.service'

@Injectable({
  providedIn: 'root'
})
export class UserService {

	private id: 						number;
	private officeId: 			number;
	private dependencyId:		number;
	private municipalityId:	number;
	private logged:					boolean = false;
	private name: 					string;
	private token: 					string;
	private position:				string;
	private email:					string;
	private permission: 		string[];
	private avatar:					string;
	private administrator:	boolean = false;

	constructor( private application: ApplicationService, private http: HttpClient, private router: Router, private cookies: CookieService  ) {

		if( this.cookies.check( this.application.getCookieName() ) ){
			this.token = this.cookies.get( this.application.getCookieName() );
			this.email = this.cookies.get( 'email' );
		}
	}

	getToken(): string{
		return this.token;
	}

	getOfficeId(): number{
		return this.officeId;
	}

	getName(){
		return this.name;
	}

	getAvatar(){
		return this.avatar;
	}

	hasPermission( permission: string ): boolean{
		if( this.administrator )
			return true;
		if( this.permission )
			return this.permission.indexOf( permission ) !== -1 ? true : false;
		else
			return false;
	}

	private clearCurrentUser(){
		this.id 						= null;
		this.officeId 			= null;
		this.dependencyId		= null;
		this.municipalityId	= null;
		this.logged 				= false;
		this.name 					= null;
		this.token 					= null;
		this.position				= null;
		this.email					= null;
		this.permission			= null;
		this.avatar					= null;
		this.administrator	= false
	}

	private updateValues( data ){
		this.id 						= data[ 'id' ];
		this.officeId 			= data[ 'officeId' ] 				|| null;
		this.dependencyId		= data[ 'notary_id'] 				|| null;
		this.municipalityId	= data[ 'municipalityId' ] 	|| null ;
		this.logged 				= true;
		this.name 					= data[ 'name' ];
		this.position				= data[ 'position' ] 				|| '';
		this.email					= data[ 'email' ];
		this.permission 		= data[ 'permission' ];
		this.avatar					= data[ 'avatar' ];
		this.administrator 	= data[ 'administrator' ] 	|| false;
		if( data[ 'token' ] )
			this.token 					= data[ 'token' ];
	}

	isLogged(){
		return this.logged;
	}

	login( email: string, password: string ){
		this.clearCurrentUser();
		this.http.post( this.application.getURL() + 'authenticate', { email: email,  password: password } )
			.subscribe(
				data => {
					this.updateValues( data );
					this.cookies.set( this.application.getCookieName(), data[ 'token' ] );
					this.cookies.set( 'email', email );
					this.router.navigate( [ '' ] );
				}
			);
	}

	checkToken(): Observable<boolean>{
		return this.http.get( this.application.getURL() + 'token' )
			.pipe(
				map( data  => {
						this.updateValues( data );
						return true;
					},
					error => { return false; }
				)
			);
	}

	logout(){
		this.clearCurrentUser();
		this.cookies.delete( this.application.getCookieName() );
		this.router.navigate( [ '/authenticate' ] );
	}

	forgotPassword( email: string ){
		return this.http.post( 'https://www.icreson.gob.mx:8444/icreson-acount/user/forgot-password', { email: email } );
	}
}
