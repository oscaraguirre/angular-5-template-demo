import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

//Custom services
import { UserService } from './user.service';
import { IcresonCommonDataService } from './icreson-common-data.service';

@Injectable()
export class HttpRequestsInterceptor implements HttpInterceptor {

	constructor( private user: UserService, private commonData: IcresonCommonDataService, private router: Router ) { }

	private icresonHost( url: string ):boolean{
		let hosts = this.commonData.getHosts();
		for( let i in hosts )
			if( url.indexOf( hosts[ i ] ) !== -1 )
				return true;
		return false;
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

		let token = this.user.getToken();
		if( token && this.icresonHost( req.url )  ){ //if request URL is for an icreson Host service, add x-access-token and catch error responses
			const icrReq = req.clone( { 	headers: req.headers.set( 'x-access-token', token ) } );

			return next.handle( icrReq )
			.pipe(
				tap(
					( event: HttpEvent<any> ) => {

					},//nothing to do here
					(err: any) => {
						console.log( err.error.message );
						if( err instanceof HttpErrorResponse &&  err.status === 403 ) { // has no permission
							//check if user are logged and show modal
						} else if ( err instanceof HttpErrorResponse &&  err.status === 401 ){
							let message = JSON.parse( err.error ).message;
							if( message == 'Expired token.' ){
								//check if user are logged
								//	for logged user show modal for password
								//	for not logged user go to authenticate
								this.router.navigate( [ '/authenticate' ] );

							} else if( message == 'Invalid token.' ){
								this.router.navigate( [ '/authenticate' ] );
							}
						}
					}
				)
			)
		} else
			return next.handle( req );
	}

}
