import { Injectable } from '@angular/core';
import { IcresonCommonDataService } from './icreson-common-data.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
	
	private service:		string = 'serviceRequests';
	private id: 				number;
	private name: 			string;
	private backendURL: string;
	private cookieName: string = 'icrServiceRequests';

	constructor( private commonData: IcresonCommonDataService ){
		this.commonData.getLoad()
			.subscribe(
				() => {},
				() => {},
				() => { this.updateData(); }
			);
	}

	public getId(){
		return this.id;
	}

	public getName(){
		return this.name;
	}

	public getURL(){
		return this.backendURL;
	}

	public getCookieName(){
		return this.cookieName;
	}

	public updateData(){
		let services 		= this.commonData.getServices();
		let hosts				= this.commonData.getHosts();
		this.id 				= services[ this.service ].applicationId;
		this.name 			= services[ this.service ].description;
		if( services[ this.service ].pipeThrough )
			this.backendURL	= hosts[ services[ this.service ].pipeThrough ] + services[ this.service ].entryPoint;
		else
			this.backendURL	= hosts[ services[ this.service ].host ] + ':' + services[ this.service ].port + services[ this.service ].entryPoint;
	}

}
