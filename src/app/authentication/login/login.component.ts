//Angular
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

//custom component
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	email: 		string;
	password: string;

  constructor( private user: UserService, private router:Router) { }

  ngOnInit() {
  }

	onLogin(){
		this.user.login( this.email, this.password );
	}

  onForgotPass(){
    console.log("olvide contraseña");
    this.router.navigate(['/forgot-password']);
  }
}
