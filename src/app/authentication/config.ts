import { Routes } from '@angular/router';

//Custom components
import { LoginComponent } from  './login/login.component';
import { ForgotComponent } from './forgot/forgot.component';

export const AuthenticationRoutes: Routes = [
  {
      path:       'authenticate',
      component:  LoginComponent
  },
  {
    path:      'forgot-password',
    component: ForgotComponent
  }
];
