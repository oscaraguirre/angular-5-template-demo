//Angular
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

//Custom component
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
 email: string;

 constructor(private user:UserService, private router:Router) { }

  ngOnInit() {
  }
  onForgotPass(){
    console.log("recuperar contraseña");
    this.user.forgotPassword(this.email);
  }
  onLogin(){
    this.router.navigate(['/authenticate']);
  }

}
