import { NgModule }     from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } 	from '@angular/forms';

//Custom modules
import { MaterialModule } from '../material/material.module';

//Custom components
import { ContainerComponent } from './container/container.component';
import { ForgotComponent }    from './forgot/forgot.component';
import { LoginComponent }     from './login/login.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [
    ContainerComponent,
    ForgotComponent,
    LoginComponent
  ]
})
export class AuthenticationModule { }
