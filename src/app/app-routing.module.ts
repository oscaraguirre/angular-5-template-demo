import { NgModule } 						from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//custom components
import { LayoutComponent } 				from './layout/layout/layout.component';
import { NotFoundComponent } 			from './default-pages/not-found/not-found.component';
import { UnauthorizedComponent } 	from './default-pages/unauthorized/unauthorized.component';
import { LoginComponent } 				from './authentication/login/login.component';

//custom services
import { AuthGuardService } 			from './services/auth-guard.service'; 							// authentication guard, user must be logged or has a cookie token.
import { PermissionGuardService } from './services/permission-guard.service';	// user has permission or is an administrator

//Custom routes
import { LayoutRoutes } 				from './layout/config';
import { AuthenticationRoutes } from './authentication/config';
import { HomeRoutes } 				from './custom-modules/home/config';

const routes: Routes = [
	{
		path: '',
		canActivate: [ AuthGuardService ],
		canActivateChild: [ AuthGuardService ],
		component: LayoutComponent,
		children:	[...HomeRoutes,
			...LayoutRoutes
		]
	},
	...AuthenticationRoutes,
	{
		path: 'unauthorized',
		component: UnauthorizedComponent
	},
	{
		path: '**',
		component: NotFoundComponent
	}
];

@NgModule({
	imports: [
		RouterModule
			.forRoot( routes )
	],
	exports: [
		RouterModule
	],
	providers: [
		AuthGuardService,
		PermissionGuardService
	]
})
export class AppRoutingModule{ }
