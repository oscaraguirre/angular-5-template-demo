import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../../../share/menu-item.interface';
import { toolItemsList } from '../../../../share/menu-items-consolidate'



@Component({
  selector: 'app-tool-items',
  templateUrl: './tool-items.component.html',
  styleUrls: ['./tool-items.component.css']
})
export class ToolItemsComponent implements OnInit {
  private toolItemsList: MenuItem[];
  position: string = "";
  constructor() {
    this.toolItemsList = toolItemsList;
  }

  ngOnInit() {
  }

}
