import { Component, OnInit ,Directive, HostListener} from '@angular/core';
import { Router } from '@angular/router';
import * as screenfull from 'screenfull';

//Custom Services
import { UserService } from '../../../../services/user.service';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css']
})
@Directive({
    selector: '[toggleFullscreen]'
})
export class UserMenuComponent implements OnInit {

	userName: string;
	avatarUrl: string;

	constructor( private user: UserService, private router: Router ) {
		this.userName = this.user.getName();
		this.avatarUrl = this.user.getAvatar();
	}


 toggleFullscreen(){
     if (screenfull.enabled) {
         screenfull.toggle();
     }
 }
  ngOnInit() {
  }

	onLogout(){
		this.user.logout();
	}

	onEditProfile(){
		this.router.navigate( ['/profile'] );
	}
}
