import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

//custom services
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit, OnDestroy {

	private onToggle: Subscription;
	public toggle: boolean = true;

	constructor( private menu: MenuService ) { 
	}

	ngOnInit() {
		this.onToggle = this.menu.getToggle()
			.subscribe(
				( toggle ) => { this.toggle = toggle; }
			);
	}

	ngOnDestroy(){
		this.onToggle.unsubscribe();
	}

}
