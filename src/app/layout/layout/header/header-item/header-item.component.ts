import { Component, OnInit } from '@angular/core';
import { LayoutComponent } from '../../layout.component';

//Custom service
import { MenuService } from "../../../menu.service";

@Component({
  selector: 'app-header-item',
  templateUrl: './header-item.component.html',
  styleUrls: ['./header-item.component.css']
})
export class HeaderItemComponent implements OnInit {

	constructor( private menu: MenuService ) {
		
	}

  ngOnInit() {
  }

	toggle( ){
		this.menu.changeStatus();
	}
}
