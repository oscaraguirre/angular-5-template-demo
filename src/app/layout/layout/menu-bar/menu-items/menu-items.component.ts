import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../../../share/menu-item.interface';
import { menuItemsList } from '../../../../share/menu-items-consolidate'



@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.css']
})
export class MenuItemsComponent implements OnInit {
  private menuItemsList: MenuItem[];
  position: string = "";
  constructor() {
    this.menuItemsList = menuItemsList;
  }

  ngOnInit() {
  }

}
