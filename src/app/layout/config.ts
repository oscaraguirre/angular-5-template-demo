import { Routes } from '@angular/router';

//Local components to route
import { UserProfileComponent } from './user-profile/user-profile.component';

export const LayoutRoutes: Routes = [
	{	
		path: 			'profile',
		component: 	UserProfileComponent
	}
];
