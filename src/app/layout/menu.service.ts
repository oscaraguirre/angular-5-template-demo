import { Subject }    from 'rxjs';

export class MenuService{

	private toggle = new Subject<boolean>();
	private value: boolean = true;

	constructor(){
	
	}

	
	changeStatus(){
		this.value = !this.value;
		this.toggle.next( this.value );	
	}

	getToggle(){
		return this.toggle.asObservable();
	}

} 
