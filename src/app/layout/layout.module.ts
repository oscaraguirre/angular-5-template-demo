//Angular modules
import { NgModule } 		from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

//Custom components
import { LayoutComponent } 			from './layout/layout.component';
import { UserMenuComponent } 		from './layout/tool-bar/user-menu/user-menu.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { MenuItemsComponent }   from './layout/menu-bar/menu-items/menu-items.component';
import { ToolItemsComponent }   from './layout/tool-bar/tool-items/tool-items.component';
//Custom modules
import { MaterialModule }	from '../material/material.module';
import {ShareModule} from '../share/share.module';
import { HeaderItemComponent } from './layout/header/header-item/header-item.component';

//Custom services
import { MenuService } from './menu.service';



@NgModule({
	declarations: [
		LayoutComponent,
		UserMenuComponent,
		UserProfileComponent ,
		MenuItemsComponent,
		ToolItemsComponent,
		HeaderItemComponent
	],
  imports: [
		CommonModule,
		MaterialModule,
		RouterModule,
		ShareModule
	],
	providers:[
		MenuService
	],
	exports:[
		LayoutComponent
	]
})
export class LayoutModule { }
