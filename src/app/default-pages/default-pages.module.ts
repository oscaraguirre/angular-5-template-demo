import { NgModule } 		from '@angular/core';
import { CommonModule } from '@angular/common';

//Custom components
import { NotFoundComponent } 			from './not-found/not-found.component';
import { UnauthorizedComponent } 	from './unauthorized/unauthorized.component';


@NgModule({
  imports: [
    CommonModule
  ],
	declarations: [
		NotFoundComponent, 
		UnauthorizedComponent 
	]
})
export class DefaultPagesModule { }
