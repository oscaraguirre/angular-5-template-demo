import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ContainerComponent } from './container/container.component';

import { QueuesService } from './queues.service';

@NgModule({
  imports: [
		CommonModule,
		MaterialModule
  ],
	declarations: [
		ContainerComponent
	],
	providers:[
		QueuesService
	],
	exports:[
		ContainerComponent
	]
})
export class QueuesModule { }
