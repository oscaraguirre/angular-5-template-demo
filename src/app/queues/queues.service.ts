import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Call } from './call';
import { UserService } from '../services/user.service';

@Injectable()
export class QueuesService {

	private url: 		 string = "http://172.30.8.15:8088/api/queues/";
	private dataUrl: string = "http://172.30.8.15:8095/filas/office/";
	types:	 { id: number, name: string }[] = [ { id: 0, name: "cargando" } ];
	windows: { id: number, name: string }[] = [ { id: 0, name: "..." } ];

	// get queues types and windows from server, values depends of user office id
	constructor( private http: HttpClient, private user: UserService ) { 
		this.http.get( this.dataUrl + this.user.getOfficeId() )
			.subscribe( 
				data => {
					if( data[ 'windows' ] )
						for( let i in data[ 'windows' ] )
							this.windows.push({
								id: 	parseInt( i ),
								name:	<string>data[ 'windows' ][ i ]
							});
					if( data[ 'types' ] )
						for( let i in data[ 'types' ] )
							this.types.push({
								id: 	parseInt( i ),
								name:	<string> data[ 'types' ][ i ]
							});
					this.windows[ 0 ].name = 'Selecciona una ventanilla';
					this.types[ 0 ].name = 'Selecciona una fila';
				},
				error => {
					console.error( error );
					this.windows[ 0 ].name = 'error';
				}
			);
	}

	// call new user from server
	getCall( typeId: number, windowId: number){
		return this.http.get<Call>( this.url + typeId  + '/window/' + windowId + '/call' );
	}

	getWaiting( typeId: number ){
		return this.http.get( this.url + typeId  + '/waitting' );
	} 

	sendRecall( call: Call ){
		return this.http.post<Call>(  this.url + 'recall', call );	
	}

}
