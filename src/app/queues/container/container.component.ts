import { Component, OnInit } from '@angular/core';
import { Call } from '../call';
import { QueuesService } from '../queues.service';

@Component({
  selector: 'queues-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
	windows: { name:string, id: number }[];
	types: { name:string, id: number }[];
	windowSelected: number = 0;
	typeSelected: number = 0;
	waiting: number;
	currentCall: Call;
	private timeToClean: number = 4000;

  constructor( private QS: QueuesService ) { }

  ngOnInit() {
		this.windows = this.QS.windows;
		this.types = this.QS.types;
  }

	onSelectWindow( index ){
		this.windowSelected = index;
	}

	onSelectType( index ){
		this.typeSelected = index;
		this.onWaiting();
	}

	onRecall(){
		this.QS.sendRecall( this.currentCall )
			.subscribe( data => {} );
	}

	onWaiting(){
		this.waiting = null;
		this.QS.getWaiting(  this.types[ this.typeSelected ].id )
			.subscribe( data => {
				this.waiting = parseInt( data[ 'count' ] );
				this.cleanQueue();
			});
	}

	private cleanQueue(): void{
		setTimeout( () => this.waiting = null, this.timeToClean );
	}

	onNewCall(){
		this.currentCall = null;
		this.waiting = null;
		this.QS.getCall( this.types[ this.typeSelected ].id, this.windows[ this.windowSelected ].id )
			.subscribe( data => {
				if( data[ 'message' ] ){
					this.waiting = 0;
					this.cleanQueue();
				} else{
					this.currentCall = new Call();
					this.currentCall.id = data.id;
					this.currentCall.letter = data.letter;
					this.currentCall.number = data.number;
					this.currentCall.window = data.window;
				}
			} );
	}
}
