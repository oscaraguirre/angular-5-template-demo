import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexModule , FlexLayoutModule }	from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatToolbarModule, MatInputModule, MatProgressSpinnerModule, MatCardModule, MatSelectModule, MatExpansionModule, MatIconModule,
	 MatGridListModule, MatTooltipModule, MatDialogModule, MatMenuModule,MatSidenavModule,MatCheckboxModule,MatFormFieldModule,MatDividerModule,MatListModule
} from '@angular/material';

@NgModule( {
	imports: [
		BrowserAnimationsModule,
		CommonModule,
		MatButtonModule,
		MatInputModule,
		MatProgressSpinnerModule,
		MatCardModule,
		MatSelectModule,
		MatExpansionModule,
		MatIconModule,
		MatGridListModule,
		MatTooltipModule,
		MatDialogModule,
		MatToolbarModule,
		MatMenuModule,
		MatSidenavModule,
		MatCheckboxModule,
		MatFormFieldModule,
		FlexModule,
		FlexLayoutModule,
		MatDividerModule,
		MatListModule
	],
	exports: [
		BrowserAnimationsModule,
		MatButtonModule,
		MatInputModule,
		MatProgressSpinnerModule,
		MatCardModule,
		MatSelectModule,
		MatExpansionModule,
		MatIconModule,
		MatGridListModule,
		MatTooltipModule,
		MatDialogModule,
		MatToolbarModule,
		MatMenuModule,
		MatSidenavModule,
		MatCheckboxModule,
		MatFormFieldModule,
		FlexModule,
		FlexLayoutModule,
		MatDividerModule,
		MatListModule
	],
	declarations: [
	]
} )
export class MaterialModule { }
